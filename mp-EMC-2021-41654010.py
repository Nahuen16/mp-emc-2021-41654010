# EMC: 2021
# Nombre del Alumno: Daniel Nahuen Zalasar
# DNI: 41654010

import os

def continuar():
    print()
    input('presione una tecla para continuar ...')
    os.system('cls')

def menu():
    print('1) Agregar un empleado')
    print('2) Mostrar los id de los empleados') #Mostrar los ID de los empleados mayores a un id solicitado por el usuario
    print('3) Modificar el departamento de un empleado')
    print('4) Ordenar en forma descendente los empleados')
    print('5) Salir')

    eleccion = int(input('Elija una opcion: '))

    while not((eleccion >= 1) and (eleccion <= 5)):
        eleccion = int(input('Elija una opcion: '))
    os.system('cls')
    return eleccion


def leerempleados():
    print('Cargar Lista de Empleados')
    empleados = {[100, "Steven King", "king@gmail.com", "Gerencia", 24000],
             [101, "Neena Kochhar", "neenaKochhar@gmail.com", "Ventas", 17000],
             [102, "Lex De Haan", "lexDeHaan@gmail.com", "Compras", 16000],
             [103, "Alexander Hunold", "alexanderHunold@gmail.com", "Compras", 9000],
             [104, "David Austin", "davidAustin@gmail.com", "Compras", 4800],
             [105, "Valli Pataballa", "valliPataballa@gmail.com", "Ventas", 4200],
             [106, "Nancy Greenberg", "nancyGreenberg@gmail.com", "Ventas", 5500],
             [107, "Daniel Faviet", "danielFaviet@gmail.com", "Ventas", 3000],
             [108, "John Chen", "johnChen@gmail.com", "Compras", 8200],
             [109, "Ismael Sciarra", "ismaelSciarra@gmail.com", "Compras", 7700],
             [110, "Alexander Khoo", "alexanderKhoo@gmail.com", "Comrpas", 3100]}
    
	#id = int(input('Ingrese el id del empleado'))

	#if type(id) != int:
	#	raise TypeError, "El id ingresado debe ser del tipo int" # valiudo primero que el tipo de dato ingresado en id sea un nro entero

	id = -1
    while (id != 0):
        id = int(input('Id (cero para finalizar): '))
        if id != 0: 
            datosid = empleados.get(dni,-1)
            if (datosid == -1):
                nombre = input('Nombre: ')
				email = input('Email: ')
				departamento = input('Departamento: ')
				salario = input('Salario: ')
                #nota = leerNota()
                estudiantes[dni] = [nombre,nota]
                print('El empleado fue agregado correctamente')
            else:
                print('El empleado ya existe')

    return empleados

def buscarid(empleados):
    id = int(input('Id: '))
    datos = empleados.get(id,-1)
    return id,datos

def buscar(id):

    print('Buscar un empleado por su id') # busco el id del empleado para no cargarlo 2 veces
    id, datos = buscarid(empleados)
    if(datos != -1): 
        print(id, datos)
    else:
        print('No existe el Id ingresado')
    
def modificar(empleados):
    print('Modificar el departamento de un empleado')
    id, datos = buscarid(empleados)    
    if (datos != -1):
        departamento = input('Nuevo departamento: ')
        departamento = leerdepartamento()
        empleados[id] = [nombre,departamento]
        print('Modificado correctamente')
    else:
        print('El empleado que busca modificar NO EXISTE')


def mayoresaunId(empleados):
    print('Lista de empleados mayores a un id solicitado: ')
    for id, datos in empleados.items():
            print(dni, datos)

def salir():
    print('Fin del programa...')

#principal
opcion = 0
os.system('cls')

while (opcion != 5):
    opcion = menu()
    if opcion == 1:
        empleados = leerempleados()
    elif opcion == 2:
        mostrar(empleados)
    elif opcion == 3:
        buscarid(empleados)
    elif opcion == 4:
        modificar(empleados)
    elif opcion == 5:
##       
        salir()
    continuar()
